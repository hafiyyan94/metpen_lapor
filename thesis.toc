\contentsline {chapter}{ABSTRAK}{ii}{section*.2}
\contentsline {chapter}{Daftar Isi}{iii}{section*.2}
\contentsline {chapter}{Daftar Gambar}{v}{section*.2}
\contentsline {chapter}{Daftar Tabel}{vi}{section*.2}
\contentsline {chapter}{\numberline {1}\uppercase {Pendahuluan}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Permasalahan}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Definisi Permasalahan}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Batasan Permasalahan}{3}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Tujuan dan Manfaat Penelitian}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Posisi Penelitian}{4}{section.1.4}
\contentsline {chapter}{\numberline {2}\uppercase {Studi Literatur}}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Aplikasi LAPOR!{}}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Metode Kuantitatif}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}Metode Kualitatif}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Metode \textit {Mixed Methods}}{12}{section.2.4}
\contentsline {section}{\numberline {2.5}Penelitian Terkait}{15}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Perbandingan penggunaan \textit {Whatsapp} dan SMS}{15}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Konsep \textit {Mindless Computing}}{17}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}\textit {Smarphone} dapat membentuk kebiasaan}{18}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Tingkat kepercayaan dalam \textit {e-participation}}{19}{subsection.2.5.4}
\contentsline {chapter}{\numberline {3}\uppercase {Metodologi Penelitian}}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Alur Pikir Penelitian}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}Rancangan Penelitian}{24}{section.3.2}
\contentsline {section}{\numberline {3.3}Tahapan Penelitian}{25}{section.3.3}
\contentsline {section}{\numberline {3.4}Instrumen Penelitian dan Pengambilan Data}{26}{section.3.4}
\contentsline {section}{\numberline {3.5}Teknik Analisis Data Penelitian}{27}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Pengolahan Data Kualitatif}{27}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Pengolahan Data Kuantitatif}{29}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}Jadwal dan Kegiatan Penelitian}{30}{section.3.6}
\contentsline {chapter}{Daftar Referensi}{32}{table.3.3}
\contentsline {chapter}{LAMPIRAN}{1}{section*.14}
\contentsline {chapter}{Lampiran 1}{2}{section*.15}
\contentsline {chapter}{\numberline {A}Transkrip Wawancara dengan KSP}{2}{chapter.1}
\contentsline {chapter}{\numberline {B}Draft pertanyaan Wawancara}{4}{chapter.2}
\contentsline {chapter}{\numberline {C}\textit {Gantt Chart}}{6}{chapter.3}
